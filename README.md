# TEMA: "Conjuntos, Aplicaciones y funciones (2002)"
```plantuml
@startmindmap
*[#lightblue] Conjuntos, Aplicaciones y funciones
	*[#Green] Conjunto
		*[#Orange] Es una colección o reunión de elementos
			* Idea intuitiva
			* Son conceptos primitivos
		*[#Orange] Inclusion de conjuntos
			* Es un trasunto de la relación de orden entre números
			* Todos los elementos del primero pertenecen al segundo
		*[#Orange] Operaciones
			* Intersección de conjuntos
				* Elementos que pertenecen simultáneamente a ambos 
			* Union
				* Elementos que pertenecen a al menos alguno de ellos
			* Complementacion
				* Son los que no pertenecen complementario de un conjunto dado
		*[#Orange] Tipos
			* Universal
				* Es un conjunto de referencia en el que\n ocurren todas las cosas de la teoría
			* Vacio
				* Es una necesidad lógica para cerrar el\n conjunto que no tiene elementos
		*[#Orange] Representaciones graficas
			* Ayudan a entender los conceptos\n y a resolver problemas
			* Se representan mediante diagramas de Venn
				* Es una coleccion de elementos como:
					* Circulos
					* Ovalos
					* Lineas cerradas
			* Se deben a Jhon Venn
				* Ayuda a conocer intuitivamente la\n posición de las operaciones
				* Es Útil para hacer sidra de cómo son las cosas
				* No sirve como método de demostración
		*[#Orange] Cardinal de un conjunto
			* Número de elementos que conforman el conjunto
				* Ejemplo: 
					* Números naturales como 1,2,3..\nsegún los elementos que tengan
			* Propiedades
				* Formula del cardinal de\n la Unión de conjuntos
					* Es igual al cardinal de uno de los conjuntos, el conjunto A\n mas el cardinal del conjunto B menos el cardinal\n de la intersección
				* Acotacion de cardinales
					* Son relaciones de:
						* Quién es mayor
						* Quién es menor
						* Quién es igual entre cardinales\n del mismo conjunto
	*[#Green] Aplicaciones
		*[#Orange] Disciplina científica qué estudia las\n transformaciones, el concepto matemático de cambios
		*[#Orange] Es una transformación o una regla convierte\n a cada uno de los elementos en un único elemento\n de un conjunto final o destino
		*[#Orange] Transformacion
			* Es un proceso de cambio, dterminado de ciertas acciones
			* Qué elementos se convierte en el segundo
		*[#Orange] Tipos de aplicaciones
			* Aplicación inyectiva
			* Aplicación sobreyectiva
			* Aplicación biyectiva
	*[#Green] Funciones
		*[#Orange] Son conjuntos que se transforman los conjuntos de numeros
		*[#Orange] Son aplicaciones fáciles de ver
			* Sistemas de coordenadas
				* Serie de puntos
				* Linea recta
				* Parabolas

@endmindmap
```

# TEMA: "Funciones (2010)"
```plantuml
@startmindmap
*[#lightblue] Funciones 
	*[#Green] Es una obligación especial
		*[#Orange] Los conjuntos que se están relacionando\n son conjuntos de números
			* Conjunto de números reales
	*[#Green] Representacion graficas
		*[#Orange] Representacion cartesiana
			* Representar en un plano
				* Un eje los valores del conjunto\n de los números reales
					* Se pueden representar en una recta
			* Eje de las imagenes
				* f(x)= 9
			* Fue creado por René Descartes
	*[#Green] Funciones crecientes
		*[#Orange] Aumenta la variable independiente del primer conjunto
		*[#Orange] Aumentan sus imágenes
	*[#Green] Funciones decrecientes
		*[#Orange] Disminuyen las variables
		*[#Orange] Aumentan sus imagenes al aumentar sus variables
	*[#Green] Comportamiento
		*[#Orange] Maximo
			* Cuándo ha pasado por el máximo de la grafica
		*[#Orange] Minimo
			* Cuándo decreció al valor mínimo de la Gráfica
	*[#Green] Limite
		*[#Orange] Cuando los valores de la función\n f(x) es tan cerca de sus imágenes
		*[#Orange] Se considera un valor que está cerca\n del punto de sus imágenes de un determinado valor
			* Es continua
			* No hay saltos en los valores
		*[#Orange] Pretende aproximar los valores 
	*[#Green] Derivadas
		*[#Orange] La idea es aproximar una función muy\n complicada a una función muy sencilla
		*[#Orange] Funciones sencillas
			* Funciones lineales
		*[#Orange] Es la aproximación de la función\n mediante la función más sencilla
		*[#Orange] Resuelve
			* El problema de la aproximación de una función compleja
@endmindmap
```

# TEMA: "La matemática del computador (2002)"
```plantuml
@startmindmap
*[#lightblue] La matemática del computador
	*[#Green] Las maematicas
		*[#Orange] Ayudan al mundo de los computadores
		*[#Orange] Nos permite
			* Ver imagenes
			* Reproducir libros
			* etc
		*[#Orange] Es una herramienta imprescindible para\n el desarrollo de las computadoras
		*[#Orange] Representación hábil de un número con\n infinitas cifras decimales
	*[#Green] Aritmetica
		*[#Orange] Calculos
			* Numero reales
			* Numero abstractos
		*[#Orange] Problemas matemáticos prácticos
			* Número finito de posiciones
			* Los números reales pierden su sentido
	*[#Green] Errores
		*[#Orange] Truncamiento
			* Cortar un número que tiene una expresión\n con infinitas cifras decimales a la derecha
			* Despreciar un término de número de cifras
		*[#Orange] Redondeo
			* Es una especie de truncamiento refinado
			* Tratan que los errores sean los mas pequeños
			* Consiste en despreciar y retocar la última cifra\n qué no despreciamos de un modo adecuado
	*[#Green] sistema de numeración
		*[#Orange] Sistemas decimal
			* Se representa en base de 10			
		*[#Orange] Sistemas binario
			* Es el sistema más simple		 
			* Se construye el sistema de numeración
				* Se construyen
					* Imagenes
					* Videos
					* Digitalizacion
			* Operaciones elementales
				* Suma
					* Base 2
				* Multiplicacion
		*[#Orange] Sistema octal
			* Representaciones compactas de números
			* Puede representar un número mediante\n bits o digitos binarios
			* Representacion
				* Interna de números enteros
				* Magnitud signo
				* Exceso
				* Complemento 2
			* Convierte un paso de corriente en posiciones de memoria
				* Numero entero
		*[#Orange] Sistema hexadecimal
			* Acortan y agrupan en forma de\n una serie de caracteres
			* Se utilizan menos símbolos
		*[#Orange] Aritmética en punto flotante
			* Sumar
			* Multiplicar
			* Representado en una notación científica
	
	
@endmindmap